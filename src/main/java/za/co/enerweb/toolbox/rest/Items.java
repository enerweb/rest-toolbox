package za.co.enerweb.toolbox.rest;

import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlRootElement
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Items<T> {

    private Meta meta;
    private Collection<T> items;

    public void add(T t) {
        items.add(t);
    }
}
