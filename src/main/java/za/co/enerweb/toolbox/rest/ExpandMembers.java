/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.co.enerweb.toolbox.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;
import org.apache.commons.lang.StringUtils;

public class ExpandMembers extends ArrayList<String> {

    public static final String PARAM = "expand";

    public static final ExpandMembers NONE = new ExpandMembers();

    private Stack<Class<?>> classStack = new Stack<Class<?>>();
    private boolean stackoverflow = false;

    private ExpandMembers() {
        super(0);
    }

    public ExpandMembers(String mainUriFragment, String paramValue) {
        this.add(mainUriFragment);
        if (StringUtils.isNotBlank(paramValue)) {
            String[] split = paramValue.split(",");
            this.addAll(Arrays.asList(split));
        }
    }

    public boolean shouldExpand(String uriFragment) {
//        return true;
        return !stackoverflow && contains(uriFragment);
    }

    public void enter(Class<?> aClass) {
        if (classStack.contains(aClass)) {
            stackoverflow = true;
        } else {
            classStack.push(aClass);
        }
    }

    public void exit(Class<?> aClass) {
        if (classStack.peek().equals(aClass)) {
            classStack.pop();
        }
        stackoverflow = false;//??add test
    }
}
