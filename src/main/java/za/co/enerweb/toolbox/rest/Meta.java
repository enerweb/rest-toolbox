/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.co.enerweb.toolbox.rest;

import java.net.URI;
import java.net.URISyntaxException;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Use MetaBuilder to construct
 */
@Data
@XmlRootElement(name = "Meta")
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Meta {
    private String href;
    private String mediaType;

//    build mediaType?!
//    or put a Meta factory in or two in ADF
    @XmlTransient
    public URI getUri() {
        try {
            return new URI(href);
        } catch (URISyntaxException e) {
            throw new RuntimeException("Invalid href: " + href, e);
        }
    }
}
