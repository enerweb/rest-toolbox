/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.co.enerweb.toolbox.rest;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class ResponsTypeUtil {
    private static final String XML = ".xml";
    private static final String JSON = ".json";

    public static final String PARAM = "responseType";
    public static final String PATH
        = "{" + PARAM + " : (\\" + XML + "|\\" + JSON + ")?}";

    public static Response.ResponseBuilder parse(Response.ResponseBuilder rb,
        String responseType) {

        if (responseType != null) {
            switch (responseType) {
                case XML:
                    rb.type(MediaType.APPLICATION_XML);
                    break;
                case JSON:
                    rb.type(MediaType.APPLICATION_JSON);
                    break;
            }
        }

        return rb;
    }

    // TODO: can probably get rest to map to this by configuring jaxon...
    public static Response.ResponseBuilder parse(Response.ResponseBuilder rb,
        String errorMessage, Throwable t,
        String responseType) {
        log.debug(errorMessage, t);
        return parse(
            rb.entity(
                new RestException(errorMessage, t)), responseType);
    }
}
