package za.co.enerweb.toolbox.rest;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import za.co.enerweb.toolbox.throwable.ThrowableUtils;

@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@XmlRootElement
public class RestException {
    String errorMessage;
    // String error;
    String trace;

    public RestException(String message, Throwable t) {
        this.errorMessage = message;
        trace = ThrowableUtils.getMessagesAsString(t);
    }
}
