/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.co.enerweb.toolbox.rest;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

public class MetaBuilder {
    private StringBuilder href = new StringBuilder();
    private StringBuilder mediaType = new StringBuilder();

    public MetaBuilder(String href) {
        this.href.append(href);
    }

    public MetaBuilder(UriInfo uriInfo) {
        this(uriInfo.getBaseUri().toString());
    }

    /**
     * appends subpath to href without prepending with a slash
     *
     * @param subpath
     * @return
     */
    public MetaBuilder add(Object subpath) {
        href.append(subpath.toString());
        return this;
    }

    /**
     * appends subpath to href after prepending with a slash
     *
     * @param subpath
     * @return
     */
    public MetaBuilder slash(Object subpath) {
        return add("/").add(subpath.toString());
    }

    public MetaBuilder applicationJson() {
        return addMediaType(MediaType.APPLICATION_JSON);
    }

    public MetaBuilder applicationXml() {
        return addMediaType(MediaType.APPLICATION_XML);
    }

    /*
     * can later add a version that can app parameters eg:
     * application/json;v=1,application/xml;v=1,
     */
    public MetaBuilder addMediaType(String type) {
        mediaType.append(type);
        mediaType.append(";,");
        return this;
    }

    public Meta build() {
        return new Meta(href.toString(), mediaType.toString());
    }
}
